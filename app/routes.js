// app/routes.js

var sess;
var mongoose = require('mongoose');
var configDB = require('../database/database.js');
//var WebHDFS = require('webhdfs');
//var hdfs = WebHDFS.createClient();
//var remoteFileStream = hdfs.createWriteStream('/path/to/remote/file');

function fillRecs(recs, i, username, res){
	var Recipe = require('../models/recipe');
	if(i<15){
		Recipe.count().exec(function (err, count) {
			// Get a random entry
			var random = Math.floor(Math.random() * count);
			// Again query all recipes but only fetch one offset by our random #
			Recipe.findOne().skip(random).exec(function (err, result){
				recs[i] = result;
				i++;
				fillRecs(recs, i, username, res);
			});
		});
	}else{
		res.render('mainpage.ejs', {
    		user : username,
    		recipes : recs
    	});
	}
}

module.exports = function(app, passport) {
	
	// =========================================
	// ENTRY ROUTE =============================
	// - login page (if user is logged) ========
	// - profile page (if user is not logged) ==
	// =========================================
	app.get('/', function(req, res) {
		sess = req.session; //Session set when user Request our app via URL
		if(sess.email) { //check Session existence
			res.redirect('/mainpage'); //redirect to user profile route
		}else {
			if(mongoose.connection.readyState==1){
				mongoose.connection.close();
			}else if(mongoose.connection.readyState==0){
				mongoose.Promise = global.Promise;
	        	mongoose.connect(configDB.url,{ server: { poolSize: 5 } }); // connect to our database
				res.render('index.ejs'); // load the index.ejs file
			}else{
				res.redirect('/');
			}
		}
	});


	// =====================================
	// MAIN PAGE ROUTE =====================
	// =====================================
    app.get('/mainpage', function(req, res) {
    	sess = req.session; //Session set when user Request our app via URL
		var recs = new Array(15);
		if(sess.email)
			fillRecs(recs, 0, sess.name, res);
		else
			fillRecs(recs, 0, null, res);
    });

    // =====================================
	// FACEBOOK AUTH ROUTE =================
    // route for facebook authentication ===
	// =====================================
	app.get('/auth/facebook', passport.authenticate('facebook', { scope: [	'email', 'user_birthday', 'user_location'	]}));

	
	// =====================================
	// FACEBOOK CALLBACK HANDLER ROUTE =====
    // route for facebook callback =========
	// =====================================
	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect : '/savesession',
			failureRedirect : '/'
	}));

	// =====================================
	// SAVE SESSION ROUTE ===================
    // route for savesession page =========
	// =====================================
	app.get('/savesession', function(req, res) {
		sess = req.session;
		sess.email = req.user._id;
		if(req.user.facebook.name){
			sess.name = req.user.facebook.name;
		}else{
			sess.name = req.user.local.name;
		}
		res.redirect('/mainpage');
	});
	
	// =====================================
	// SIGNIN PAGE ROUTE ===================
    // route for signin page ===============
	// =====================================
	app.get('/signin', function(req, res) {
		res.render('signin.ejs', { message: req.flash('loginMessage') });
	});
	
	// process the login form
	app.post('/signin', passport.authenticate('local-login', {
		successRedirect : '/savesession', // redirect to the secure profile section
		failureRedirect : '/signin', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	
	// =====================================
	// REGISTER PAGE ROUTE =================
    // route for register page =============
	// =====================================
	app.get('/register', function(req, res) {
		res.render('register.ejs', {message: req.flash('registerMessage')});
	});
	
	/* Handle Registration POST */
	app.post('/register', passport.authenticate('local-register', {
	    successRedirect: '/register',
	    failureRedirect: '/register',
	    failureFlash : true 
	}));
	
	
	// =====================================
	// LOGOUT ROUTE ========================
	// =====================================
	app.get('/logout', function(req, res) {
		req.session.destroy(function(err) { //reset user current session
			  if(err) {
			    console.log(err);
			  } else {
				  mongoose.connection.close();
				  res.redirect('/'); //redirect to entry route
			  }
		});
	});
};
