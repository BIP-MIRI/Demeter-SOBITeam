// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {

	'facebookAuth' : {
		'clientID' 		: '1011233099003815', // your App ID
		'clientSecret' 	: '3bca0793845167a6fe6599405263dea4', // your App Secret
		'callbackURL' 	: 'https://nameless-plains-50100.herokuapp.com/auth/facebook/callback'
	},

	/*'twitterAuth' : {
		'consumerKey' 		: 'your-consumer-key-here',
		'consumerSecret' 	: 'your-client-secret-here',
		'callbackURL' 		: 'http://localhost:8080/auth/twitter/callback'
	},

	'googleAuth' : {
		'clientID' 		: 'your-secret-clientID-here',
		'clientSecret' 	: 'your-client-secret-here',
		'callbackURL' 	: 'http://localhost:8080/auth/google/callback'
	}*/

};