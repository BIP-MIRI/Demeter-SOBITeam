// app/models/recipe.js
var mongoose = require('mongoose');
var configDB = require('../database/database.js');

var Ingredient = require('./ingredient');

var recipeSchema = new mongoose.Schema({
	_id     		: Number,
	title   		: String,
	readyInMinutes	: Number,
	image			: String,
	instructions	: String,
	ingredients 	: ['Ingredient']
});

var Recipe = mongoose.model('Recipe', recipeSchema);

//get 15 random recipes
/*recipeSchema.methods.getRandomRecipes = function() {
	return Recipe.findRandom().limit(15).exec(function (err, result) {
		if(err){
			console.log("HHHHHHHHH"+err);
			return err;
		} 
		console.log("AHHAHDHAHDHSH"+result);
		return result;
	});
};*/

module.exports  = Recipe;