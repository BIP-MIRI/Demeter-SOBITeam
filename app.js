// app.js

// set up ======================================================================
// get all the tools we need
var express = require('express');
var session = require('express-session');
var app      = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var flash    = require('connect-flash');

/*-----------------------------------------------------------------*/

app.set('view engine', 'ejs'); // set up ejs for templating

/*-------------------------------------------------------------------*/

require('./middleware/passport')(passport); // pass passport for configuration

//parse application/json
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(logger('dev'));

// required for session
app.use(session({ secret: "keyboard cat",
    proxy: true,
    resave: true,
    saveUninitialized: true })); // session secret

// required for passport
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

//Initialize the app.
var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport